# Nginx conf file

This repository contains **nginx.conf** file of [Nginx](http://nginx.org/).

## Getting Started

These instructions will get you a copy of nginx.conf file for Nginx web server.

### Prerequisites

You can install and run Nginx on linux server on in docker.

### Installing

How to install Nginx.

* Using docker:
```
docker run -d -p 80:80 nginx:latest
```
* Using apt package manager on ubuntu:
```
sudo apt install -y nginx
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.rebrainme.com/casablancaukraine/rebrain-devops-task-checkout/-/tags). 

## Versions

* **18.12.19:** - Added nginx.conf file.
* **18.12.19:** - The test tag/item for list.

## Authors

* **Sergii Kostiuk** - *Initial work* - [serg4kostiuk](https://github.com/serg4kostiuk)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under my License [LICENSE.md](LICENSE.md) file for details


